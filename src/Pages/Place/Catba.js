import Page from "../../Page";
import CatbaJSON from "../../Data/Place/Catba";

const Catba = props => {
  return Page({ page: CatbaJSON });
};

export default Catba;