import Page from "../../Page";
import VinpearlnhatrangJSON from "../../Data/Place/Vinpearlnhatrang";

const Vinpearlnhatrang = props => {
  return Page({ page: VinpearlnhatrangJSON });
};

export default Vinpearlnhatrang;