import Page from "../../Page";
import BunbohueJSON from "../../Data/Food/Bunbohue";

const Bunbohue = props => {
  return Page({ page: BunbohueJSON });
};

export default Bunbohue;
