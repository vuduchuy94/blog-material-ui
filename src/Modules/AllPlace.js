import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Header from "./Header";
import Footer from "./Footer";
import CatbaImage from "../Img/Places/Catba/catba4.jpg";
import SapaImage from "../Img/Places/Sapa/sapa.jpg";
import SondoongImage from "../Img/Places/Sondoong/sd-pano.jpg";
import VinpearlnhatrangImage from "../Img/Places/Vinpearlnhatrang/vinpearl-header.jpg";
import PageIndexCard from "../Modules/Views/PageIndexCard";

const useStyles = makeStyles({
  root: {
    flexFlow: 1
  }
});

const allPlaces=[
  {id:"catba", title:"Cát Bà", image: CatbaImage},
  {id:"sapa", title:"Sapa", image:SapaImage},
  {id:"sondoong", title:"Son Doong", image:SondoongImage},
  {id:"vinpearlnhatrang", title:"Vinpearl Nha Trang", image:VinpearlnhatrangImage}
];

function AllPlaces(props){
  const classes = useStyles();
  
  return (
    <React.Fragment>
    <Header />
    <Grid className={classes.root} spacing={1}>
        <Grid container justify="center" item xs = {12}>
          {allPlaces.map(place => (
            <Grid>
              <PageIndexCard parentPath={"place"} data={place}/>
            </Grid>
          ))}
        </Grid>
     </Grid> 
     <Footer />
     </React.Fragment>
  )
};

export default AllPlaces;