import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Header from "./Header";
import Footer from "./Footer";
import BunbohueImage from "../Img/Foods/Bunbohue/bunbohue1.jpg";
import ToheImage from "../Img/Foods/Tohe/tohe.jpg";
import PageIndexCard from "../Modules/Views/PageIndexCard";

const useStyles = makeStyles({
  root: {
    flexFlow: 1
  }
});

const allFoods=[
  {id:"tohe", title:"Tò he", image: ToheImage},
  {id:"bunbohue", title:"ブンボーフェ", image: BunbohueImage}
];

function AllFoods(props){
 
  const classes = useStyles();
  
  return (
    <React.Fragment>
    <Header />
    <Grid className={classes.root} spacing={1}>
        <Grid container justify="center" item xs = {12}>
          {allFoods.map(food => (
            <Grid>
              <PageIndexCard parentPath={"food"} data={food}/>
            </Grid>
          ))}
        </Grid>
     </Grid> 
     <Footer/>
    </React.Fragment>
  )
};

export default AllFoods;
