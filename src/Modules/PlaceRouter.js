import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import AllPlace from "./AllPlace";
import Catba from "../Pages/Place/Catba";
import Sapa from "../Pages/Place/Sapa";
import Sondoong from "../Pages/Place/Sondoong";
import Vinpearlnhatrang from "../Pages/Place/Vinpearlnhatrang";
const PlaceRouter = () => (
  <BrowserRouter>
    <Switch>
    <Route exact path="/place" component={AllPlace} />
    <Route path="/place/catba" component={Catba} />
    <Route path="/place/sapa" component={Sapa} />
    <Route path="/place/sondoong" component={Sondoong} />
    <Route path="/place/vinpearlnhatrang" component={Vinpearlnhatrang} />
  </Switch></BrowserRouter>
);
export default PlaceRouter;
