import React from "react";
import { Link, useHistory } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles({
  root: {
    flexFlow: 1
  },
  card: {
    margin: "30px 30px",
    padding: "10px",
    maxWidth: "300px"
  },
  media: {
    maxHeight: "150px",
  },
  content: {
            margin: "-5px 0 -10px 0",
  }
});
function PageIndexCard(props){
    const classes = useStyles();
    let history = useHistory();
    function addHistory(props){
        history.push(props.parentPath+"/"+props.data.id)
        }
    return(
      <Card onClick={() => addHistory(props)} className={classes.card}>
      <CardMedia
        component="img"
        className={classes.media}
        image = {props.data.image}
        title={props.data.title}
      />
      <CardContent className = {classes.content}>
        <Typography variant="h5" color="primary">
        {props.data.title}
        </Typography>
      </CardContent>
        <Button size="small" color="primary">
          <Link to={`${"/" + props.parentPath+"/"+ props.data.id}`}>View More</Link>
        </Button>
     </Card>
    )
  }
  export default PageIndexCard;